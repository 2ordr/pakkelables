﻿using System.Web.Routing;
using Nop.Core.Domain.Common;
using Nop.Core.Plugins;
using Nop.Services.Shipping;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using Nop.Services.Shipping;

namespace Nop.Services.Shipping
{
    public partial interface IShippingLabel 
    {
           /// <summary>
        /// Gets a route for provider configuration
        /// </summary>
        /// <param name="actionName">Action name</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="routeValues">Route values</param>
      //  void GetConfigurationRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues);

        /// <summary>
        /// Inserts a shipment
        /// </summary>
        /// <param name="shipment">Shipment</param>
      void InsertShipmentLabel(ShipmentLabel shipment);

        /// <summary>
        /// Fetches the label Path
        /// </summary>
        /// <param name="shippingId"></param>
        /// <returns></returns>
      ShipmentLabel GetLabelPath(string TrackingNumber,int OrderId);
        
     
    }
}
