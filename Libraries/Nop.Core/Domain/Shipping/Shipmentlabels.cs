using System;
using System.Collections.Generic;
using Nop.Core.Domain.Orders;

namespace Nop.Core.Domain.Shipping
{
    /// <summary>
    /// Represents a shipment
    /// </summary>
    public partial class ShipmentLabel : BaseEntity
    {
    
  
        /// <summary>
        /// Gets or sets the order identifier
        /// </summary>
        public  int OrderId { get; set; }
        
        /// <summary>
        /// Gets or sets the tracking number of this shipment
        /// </summary>
        public  string TrackingNumber { get; set; }

        /// <summary>
        /// Gets or sets the label order  Idof this shipment
        /// </summary>
        public  string LabelOrderId { get; set; }

        /// <summary>
        /// Gets or sets the ShippingId this shipment
        /// </summary>
        public  string ShippingId { get; set; }
        /// <summary>
        /// Gets or sets the packageno this shipment
        /// </summary>
        public  string PackageNo { get; set; }


        /// <summary>
        /// Gets or sets the admin comment
        /// </summary>
        public   string AdminComment { get; set; }

        /// <summary>
        /// Gets or sets the entity creation date
        /// </summary>
        public   DateTime CreationDate { get; set; }

        /// <summary>
        /// Gets or sets the pdf file path
        /// </summary>
   //     public string PDFfiePath { get; set; }
    }
}