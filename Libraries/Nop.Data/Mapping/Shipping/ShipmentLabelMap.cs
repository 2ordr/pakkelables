using Nop.Core.Domain.Shipping;

namespace Nop.Data.Mapping.Shipping
{
    public partial class ShipmentLabelMap : NopEntityTypeConfiguration<ShipmentLabel>
    {
        public ShipmentLabelMap()
        {
            this.ToTable("ShipmentLabel");
            this.HasKey(s => s.Id);

            this.Property(s => s.OrderId);
            this.Property(s => s.ShippingId).HasMaxLength(1000);
            this.Property(s => s.PackageNo).HasMaxLength(1000);
            this.Property(s => s.AdminComment);
            this.Property(s => s.LabelOrderId);
            this.Property(s => s.TrackingNumber);
            this.Property(s => s.CreationDate);
         //   this.Property(s=> s.PDFfiePath);
            
          
        }
    }
}