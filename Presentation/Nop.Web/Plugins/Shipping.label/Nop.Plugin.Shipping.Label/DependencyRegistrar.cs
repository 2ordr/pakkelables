﻿using Autofac;
using Autofac.Core;
using Nop.Core.Configuration;
using Nop.Core.Data;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Web.Framework.Mvc;
using Nop.Services.Shipping;
using Nop.Plugin.Shipping.Label.Data;
using Nop.Core.Domain.Shipping;
using Nop.Data;

namespace Nop.Plugin.Shipping.Label
{
    public class DependencyRegistrar :IDependencyRegistrar
    {
        /// <summary>
        /// Register services and interfaces
        /// </summary>
        /// <param name="builder">Container builder</param>
        /// <param name="typeFinder">Type finder</param>
        /// <param name="config">Config</param>
        public virtual void Register(ContainerBuilder builder, ITypeFinder typeFinder, NopConfig config)
        {
            builder.RegisterType<ShipmentLabelService>().As<IShippingLabel>().InstancePerLifetimeScope();

            //data context
            this.RegisterPluginDataContext<ShippingLabelObjectContext>(builder, "nop_object_context_shipping_label");

            //override required repository with our custom context
            builder.RegisterType<EfRepository<ShipmentLabel>>()
                .As<IRepository<ShipmentLabel>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>("nop_object_context_shipping_label"))
                .InstancePerLifetimeScope();
        }

        /// <summary>
        /// Order of this dependency registrar implementation
        /// </summary>
        public int Order
        {
            get { return 1; }
        }
    }
}
