﻿using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Shipping.Label.Model
{
 public   class ConfigurationModel : BaseNopModel
    {

     public int ActiveStoreScopeConfiguration { get; set; }

     [NopResourceDisplayName("Plugins.Shipping.Label.ApiUser")]
     public string ClientUser { get; set; }

     [NopResourceDisplayName("Plugins.Shipping.Label.ApiKey")]
     public string ClientKey { get; set; }
    }

}
