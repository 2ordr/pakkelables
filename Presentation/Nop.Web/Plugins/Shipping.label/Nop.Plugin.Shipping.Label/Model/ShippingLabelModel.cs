﻿using System;
using System.Collections.Generic;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Shipping.Label.Model
{
  public  class ShippingLabelModel : BaseNopModel
    {

      [NopResourceDisplayName("Plugins.Payment.Shipping.Label.ReceiverName")]
      public string ReceiverName { get; set; }

      [NopResourceDisplayName("Plugins.Payment.Shipping.Label.Address")]
      public string Address { get; set; }
      
    }
}
