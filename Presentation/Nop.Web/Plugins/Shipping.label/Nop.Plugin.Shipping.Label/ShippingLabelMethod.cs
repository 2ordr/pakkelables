﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Routing;
using System.Xml;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Stores;
using System.Threading.Tasks;
using Nop.Core.Domain.Shipping;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Shipping;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using Nop.Core.Plugins;
using Nop.Core.Data;
using Nop.Services.Shipping;
using Nop.Plugin.Shipping.Label.Data;

namespace Nop.Plugin.Shipping.Label 
{
 public partial   class ShippingLabelMethod : BasePlugin,IShippingLabel
    {

      private readonly ISettingService _settingService;
      private readonly IShippingLabel _shippingLabel;
      private readonly IRepository<Order> _orderRepository;
      private readonly ShippingLabelObjectContext _objectContext;

       public ShippingLabelMethod(ISettingService settingService,IRepository<Order> orderRepository,IShippingLabel shippingLabel,ShippingLabelObjectContext objectContext)
        {
            this._settingService = settingService;
            this._orderRepository = orderRepository;
            this._shippingLabel = shippingLabel;
            this._objectContext = objectContext;
        }


        /// <summary>
        /// Gets a route for provider configuration
        /// </summary>
        /// <param name="actionName">Action name</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="routeValues">Route values</param>
        public void GetConfigurationRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "Configure";
            controllerName = "ShippingLabel";
            routeValues = new RouteValueDictionary { { "Namespaces", "Nop.Plugin.Shipping.Label.Controllers" }, { "area", null } };
        }

        /// <summary>
        /// Install plugin
        /// </summary>
        public override void Install()
        {
            _objectContext.Uninstall();
            //database objects
            _objectContext.Install();
            //locales
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.Label.ReceiverName", "Name");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.Label.Address", "Address");

            base.Install();
        }


        /// <summary>
        /// Uninstall plugin
        /// </summary>
        public override void Uninstall()
        {
            _objectContext.Uninstall();
            //locales
            this.DeletePluginLocaleResource("Plugins.Shipping.Label.ReceiverName");
            this.DeletePluginLocaleResource("Plugins.Shipping.Label.Address");

            base.Uninstall();
        }

        public void InsertShipmentLabel(ShipmentLabel shipmentLabel)
        {
            string abc = "";
        }
        public ShipmentLabel GetLabelPath(string TrackingNumber, int OrderId)
        {
            ShipmentLabel label = new ShipmentLabel();
            return label;
        }
      
   
    }
}
