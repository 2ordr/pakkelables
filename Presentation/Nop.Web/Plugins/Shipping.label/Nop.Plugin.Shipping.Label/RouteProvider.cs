﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Mvc.Routes;

namespace Nop.Plugin.Shipping.Label
{
  public partial  class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute("Plugin.Shipping.Label.Configure",
                 "Plugins/Shipping/Label/Configure",
                 new { controller = "ShippingLabel", action = "Configure", },
                 new[] { "Nop.Plugin.Shipping.Label.Controller" }
            );

            //routes.MapRoute("Plugin..Shipping.Label.Edit",
            //     "Plugins/PickupInStore/Edit",
            //     new { controller = "PickupInStore", action = "Edit" },
            //     new[] { "Nop.Plugin.Shipping.Label.Controller" }
            //);
        }
        public int Priority
        {
            get
            {
                return 0;
            }
        }
    }
}
