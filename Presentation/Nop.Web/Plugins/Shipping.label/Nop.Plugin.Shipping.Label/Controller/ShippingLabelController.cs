﻿using System.Collections.Generic;
using System.Web.Mvc;
using Nop.Core;
using Nop.Plugin.Shipping.Label.Model;
using Nop.Services.Configuration;
using Nop.Services.Security;
using Nop.Services.Shipping;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework.Security;
using Nop.Services.Localization;
using Nop.Core.Domain.Shipping;
 


namespace Nop.Plugin.Shipping.Label.Controller
{
      [AdminAuthorize]
  public  class ShippingLabelController : BasePluginController
    {
        private readonly IShippingLabel _shippingLabel;
        private readonly ShippingLabelSetting _shippingLabelSetting;
        private readonly ILocalizationService _localizationService;
        private readonly ISettingService _settingService;

        public ShippingLabelController(IShippingLabel shippingLabel,
            ILocalizationService localizationService,ShippingLabelSetting shippingLabelSetting)
        {
            this._shippingLabel = shippingLabel;
            this._localizationService = localizationService;
            this._shippingLabelSetting = shippingLabelSetting;
        }

          [ChildActionOnly]
        public ActionResult Configure()
        {
            var model = new ConfigurationModel();
            model.ClientUser = _shippingLabelSetting.ApiUser;
            model.ClientKey = _shippingLabelSetting.ApiKey;
            return View("~/Plugins/Shipping.Label/Views/ShippingLabel/Configure.cshtml");
        }

          [HttpPost]
          [ChildActionOnly]
          public ActionResult Configure(ConfigurationModel model)
          {
              if (!ModelState.IsValid)
              {
                  return Configure();
              }

              //save settings
              _shippingLabelSetting.ApiUser = model.ClientUser;
              _shippingLabelSetting.ApiKey = model.ClientKey;
              _settingService.SaveSetting(_shippingLabelSetting);

              SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));

              return Configure();
          }


    }
}
