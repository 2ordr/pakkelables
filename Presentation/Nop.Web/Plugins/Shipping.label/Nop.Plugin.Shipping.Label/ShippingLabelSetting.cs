﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Shipping.Label
{
 public  class ShippingLabelSetting : ISettings
    {
        public string ApiUser { get; set; }

        public string ApiKey { get; set; }
    }
}
