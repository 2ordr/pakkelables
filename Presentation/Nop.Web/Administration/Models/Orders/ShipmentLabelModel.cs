﻿using System;
using System.Collections.Generic;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Admin.Models.Orders
{
    public partial class ShipmentLabelModel : BaseNopEntityModel
    {
        [NopResourceDisplayName("Admin.Orders.ShipmentLabels.ID")]
        public override int Id { get; set; }
        [NopResourceDisplayName("Admin.Orders.ShipmentLabels.OrderID")]
        public int OrderId { get; set; }
        [NopResourceDisplayName("Admin.Orders.ShipmentLabels.LabelOrderId")]
        public string LabelOrderId { get; set; }
        [NopResourceDisplayName("Admin.Orders.ShipmentLabels.TrackingNumber")]
        public string TrackingNumber { get; set; }

        [NopResourceDisplayName("Admin.Orders.ShipmentLabels.ShippingId")]
        public string ShippingId { get; set; }

        [NopResourceDisplayName("Admin.Orders.ShipmentLabels.PackageNo")]
        public string PackageNo { get; set; }

        [NopResourceDisplayName("Admin.Orders.ShipmentLabels.AdminComment")]
        public string AdminComment { get; set; }

        [NopResourceDisplayName("Admin.Orders.ShipmentLabels.CreationDate")]
        public string CreationDate { get; set; }

        [NopResourceDisplayName("Admin.Orders.ShipmentLabels.PDFfiePath")]
        public byte[] PDFfiePath { get; set; }

  
    }
}