CREATE TABLE [dbo].[PakkeLabels](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [varchar](100)  NULL,
	[ShippingID] [varchar] (50)  NULL,
	[LabelOrderId] [varchar] (50)  NULL,
	[PkgNo] [varchar](50) NULL,
	[CreationDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO